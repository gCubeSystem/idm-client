This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

# Changelog for Identity Manager Service


## [v0.0.2-SNAPSHOT] 

- Alfa version

## [v0.0.1-SNAPSHOT] 

- First Prototype

