package org.gcube.idm.client;

import java.net.URI;
import java.net.URISyntaxException;

import org.gcube.idm.client.clients.IdmRestClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AbstractIdmClient  {
    protected static Logger logger = LoggerFactory.getLogger(AbstractIdmClient.class);

    protected final static String AUTHORIZATION_HEADER = "Authorization";
    protected String auth_token;

    protected IdmRestClient client;

    public IdmRestClient getClient() {
        return client;
    }

    public void setClient(IdmRestClient client) {
        this.client = client;
    }

    public AbstractIdmClient(IdmRestClient client) throws URISyntaxException {
        this.setClient(client);
    }

    public AbstractIdmClient(String base_url) throws URISyntaxException {
        this.setClient(new IdmRestClient(base_url));
    }

    public AbstractIdmClient(URI base_url) {
        this.setClient(new IdmRestClient(base_url));
    }
}
