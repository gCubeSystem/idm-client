package org.gcube.idm.client;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.gcube.com.fasterxml.jackson.databind.JavaType;
import org.gcube.idm.client.beans.ResponseBean;
import org.gcube.idm.client.clients.IdmRestClient;
import org.gcube.idm.client.model.OwnerInfo;
import org.gcube.idm.client.model.UserInfo;
import org.gcube.idm.client.model.UserInspect;
import org.gcube.idm.client.model.UserProfile;
import org.gcube.idm.client.model.util.JsonUtilsCustom;
import org.gcube.idm.common.models.IdmUser;
import org.gcube.idm.common.models.IdmVerifyObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jakarta.ws.rs.BadRequestException;
import jakarta.ws.rs.NotAuthorizedException;

public class DefaultMeClient extends AbstractIdmClient implements IdmMeClient {
    protected String auth_token;

    public DefaultMeClient(IdmRestClient client, String auth_token) throws URISyntaxException {
        super(client);
        this.auth_token = auth_token;
    }

    public DefaultMeClient(String base_url, String auth_token) throws URISyntaxException {
        super(base_url);
        this.auth_token = auth_token;
    }

    public DefaultMeClient(URI base_url, String auth_token) throws URISyntaxException {
        super(base_url);
        this.auth_token = auth_token;
    }

    protected static Logger logger = LoggerFactory.getLogger(DefaultMeClient.class);

    @Override
    public String getMeId() {
        ResponseBean<String> resp = this.client.performGetRequest("users/me/id", auth_token, String.class);
        return resp.getResult();
    }

    @Override
    public String getMeEmail() {
        ResponseBean<String> resp = this.client.performGetRequest("users/me/email", auth_token, String.class);
        return resp.getResult();
    }

    @Override
    public String getMeUsername() {
        ResponseBean<String> resp = this.client.performGetRequest("users/me/username", auth_token, String.class);
        return resp.getResult();
    }

    @Override
    public OwnerInfo getMeOwner() {
        ResponseBean<OwnerInfo> resp = this.client.performGetRequest("users/me/owner", auth_token, OwnerInfo.class);
        return resp.getResult();
    }

    @Override
    public UserProfile getMeProfile() {
        ResponseBean<UserProfile> resp = this.client.performGetRequest("users/me/profile", auth_token,
                UserProfile.class);
        return resp.getResult();
    }

    @Override
    public IdmVerifyObject verifyToken() {
        ResponseBean<IdmVerifyObject> resp = this.client.performGetRequest("users/me/verify", auth_token,
                IdmVerifyObject.class);
        return resp.getResult();
    }

    @Override
    public String getMeName() throws NotAuthorizedException, BadRequestException {
        ResponseBean<String> resp = this.client.performGetRequest("users/me/name", auth_token, String.class);
        return resp.getResult();
    }

    @Override
    public Map<String, List<String>> getMeAttributes() throws NotAuthorizedException, BadRequestException {
        JavaType stringListType = JsonUtilsCustom.geListOfObjectsType(String.class);
        JavaType stringType = JsonUtilsCustom.getObjectJT(String.class);
        JavaType mapOfStringList = JsonUtilsCustom.geMapOfObjectsType(stringType, stringListType);

        ResponseBean<Map<String, List<String>>> resp = this.client.performGetRequest("users/me/attributes", auth_token,
                mapOfStringList);
        return resp.getResult();
    }

    // @Override
    // public String getMeAttribute(String attribute) throws NotAuthorizedException,
    // BadRequestException {
    // // TODO Auto-generated method stub
    // throw new UnsupportedOperationException("Unimplemented method
    // 'getMeAttribute'");
    // }

    @Override
    public IdmUser getMeUser() throws NotAuthorizedException, BadRequestException {
        ResponseBean<IdmUser> resp = this.client.performGetRequest("users/me/user", auth_token, IdmUser.class);
        return resp.getResult();
    }

    @Override
    public UserInfo getMe() throws NotAuthorizedException, BadRequestException {
        ResponseBean<UserInfo> resp = this.client.performGetRequest("users/me", auth_token, UserInfo.class);
        return resp.getResult();
    }

    @Override
    public UserInspect getMeInspect() throws NotAuthorizedException, BadRequestException {
        HashMap<String, String> params = new HashMap<>();
        params.put(("inspect"), "true");
        HashMap<String, String> headers = IdmRestClient.getHeadersWithAuth(auth_token, null);
        ResponseBean<UserInspect> resp = this.client.performGetRequest("users/me", headers, params, UserInspect.class);
        return resp.getResult();
    }

}
