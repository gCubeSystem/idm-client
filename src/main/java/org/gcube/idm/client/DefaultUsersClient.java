package org.gcube.idm.client;

import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.gcube.com.fasterxml.jackson.databind.JavaType;
import org.gcube.idm.client.beans.ResponseBean;
import org.gcube.idm.client.clients.IdmRestClient;
import org.gcube.idm.client.model.UserInfo;
import org.gcube.idm.client.model.UserProfile;
import org.gcube.idm.client.model.util.JsonUtilsCustom;
import org.gcube.idm.common.models.IdmFullUser;
import org.gcube.idm.common.models.IdmUser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jakarta.ws.rs.BadRequestException;
import jakarta.ws.rs.NotAuthorizedException;

public class DefaultUsersClient extends AbstractIdmClient implements IdmUsersClient {
    protected String auth_token;

    public String pathForUserID(String user_id, String extra_path) {
        try {
            user_id = URLEncoder.encode(user_id, StandardCharsets.UTF_8.toString());
        } catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            throw new BadRequestException("cannot encode the user_id " + user_id);
        }
        if (extra_path != null) {
            return String.format("/users/%s/%s", user_id, extra_path);
        }
        return String.format("/users/%s", user_id);
    }

    public DefaultUsersClient(IdmRestClient client, String auth_token) throws URISyntaxException {
        super(client);
        this.auth_token = auth_token;
    }

    public DefaultUsersClient(String base_url, String auth_token) throws URISyntaxException {
        super(base_url);
        this.auth_token = auth_token;
    }

    public DefaultUsersClient(URI base_url, String auth_token) throws URISyntaxException {
        super(base_url);
        this.auth_token = auth_token;
    }

    protected static Logger logger = LoggerFactory.getLogger(DefaultUsersClient.class);

    @Override
    public String getUserId(String user_id) {
        ResponseBean<String> resp = this.client.performGetRequest(pathForUserID(user_id, "id"), this.auth_token,
                String.class);
        return resp.getResult();
    }

    @Override
    public String getUserEmail(String user_id) {
        ResponseBean<String> resp = this.client.performGetRequest(pathForUserID(user_id, "email"), this.auth_token,
                String.class);
        return resp.getResult();
    }

    @Override
    public String getUserUsername(String user_id) {
        ResponseBean<String> resp = this.client.performGetRequest(pathForUserID(user_id, "username"), this.auth_token,
                String.class);
        return resp.getResult();
    }

    @Override
    public IdmUser getUserOwner(String user_id) {
        ResponseBean<IdmUser> resp = this.client.performGetRequest(pathForUserID(user_id, "owner"), this.auth_token,
                IdmUser.class);
        return resp.getResult();
    }

    @Override
    public UserProfile getUserProfile(String user_id) {
        ResponseBean<UserProfile> resp = this.client.performGetRequest(pathForUserID(user_id, "profile"),
                this.auth_token,
                UserProfile.class);
        return resp.getResult();
    }

    @Override
    public List<String> getAllUsernames() {
        // TODO Auto-generated method stub
        return getAllUsernames(null, null);
    }

    @Override
    public List<String> getAllUsernames(Integer first, Integer max) {
        JavaType listType = JsonUtilsCustom.geListOfObjectsType(String.class);
        ResponseBean<List<String>> resp = this.client.performGetRequest("/users/search", this.auth_token, listType);
        return resp.getResult();
    }

    @Override
    public UserInfo getUser(String user_id) {
        ResponseBean<UserInfo> resp = this.client.performGetRequest(pathForUserID(user_id, null),
                this.auth_token,
                UserInfo.class);

        return resp.getResult();
    }

    private HashMap<String, String> getSearchParameters(IdmUser.USERS_REPR format, Integer first, Integer max,
            SearchUsersParams params) {
        HashMap<String, String> parameters = new HashMap<String, String>();
        parameters.put("format", format.toString());
        if (first != null) {
            parameters.put("first", first.toString());
        }
        if (max != null) {
            parameters.put("max", max.toString());
        }
        if (params != null) {
            if (params.exact != null)
                parameters.put("exact", params.exact.toString());
            if (params.enabled != null)
                parameters.put("enabled", params.enabled.toString());
            if (params.username != null)
                parameters.put("username", params.username.toString());
            if (params.firstName != null)
                parameters.put("firstName", params.firstName.toString());
            if (params.lastName != null)
                parameters.put("lastName", params.lastName.toString());
            if (params.email != null)
                parameters.put("email", params.email.toString());
        }
        return parameters;
    }

    @Override
    public List<String> searchUsernames(Integer first, Integer max, SearchUsersParams params) {
        HashMap<String, String> headers = IdmRestClient.getHeadersWithAuth(this.auth_token, null);
        HashMap<String, String> parameters = getSearchParameters(IdmUser.USERS_REPR.username, first, max, params);
        JavaType listType = JsonUtilsCustom.geListOfObjectsType(String.class);
        ResponseBean<List<String>> resp = this.client.performGetRequest("/users/search", headers, parameters, listType);
        return resp.getResult();
    }

    @Override
    public List<IdmUser> searchUsers(Integer first, Integer max, SearchUsersParams params) {
        HashMap<String, String> headers = IdmRestClient.getHeadersWithAuth(this.auth_token, null);
        HashMap<String, String> parameters = getSearchParameters(IdmUser.USERS_REPR.compact, first, max, params);
        JavaType listType = JsonUtilsCustom.geListOfObjectsType(UserInfo.class);
        ResponseBean<List<IdmUser>> resp = this.client.performGetRequest("/users/search", headers, parameters,
                listType);
        return resp.getResult();
    }

    @Override
    public List<IdmFullUser> searchFullUsers(Integer first, Integer max, SearchUsersParams params) {
        // TODO Auto-generated method stub
        HashMap<String, String> headers = IdmRestClient.getHeadersWithAuth(this.auth_token, null);
        HashMap<String, String> parameters = getSearchParameters(IdmUser.USERS_REPR.full, first, max, params);
        JavaType listType = JsonUtilsCustom.geListOfObjectsType(IdmFullUser.class);
        ResponseBean<List<IdmFullUser>> resp = this.client.performGetRequest("/users/search", headers, parameters,
                listType);
        return resp.getResult();
    }

    @Override
    public List<String> searchEmails(Integer first, Integer max, SearchUsersParams params) {
        HashMap<String, String> headers = IdmRestClient.getHeadersWithAuth(this.auth_token, null);
        HashMap<String, String> parameters = getSearchParameters(IdmUser.USERS_REPR.email, first, max, params);
        JavaType listType = JsonUtilsCustom.geListOfObjectsType(String.class);
        ResponseBean<List<String>> resp = this.client.performGetRequest("/users/search", headers, parameters, listType);
        return resp.getResult();
    }

    @Override
    public Map<String, String> searchUsernamesEmails(Integer first, Integer max, SearchUsersParams params)
            throws NotAuthorizedException, BadRequestException {
        HashMap<String, String> headers = IdmRestClient.getHeadersWithAuth(this.auth_token, null);
        HashMap<String, String> parameters = getSearchParameters(IdmUser.USERS_REPR.username_email, first, max, params);

        JavaType mapStringType = JsonUtilsCustom.geMapOfObjectsType(String.class, String.class);
        ResponseBean<Map<String, String>> resp = this.client.performGetRequest("/users/search", headers,
                parameters, mapStringType);
        return resp.getResult();
    }

    @Override
    public Map<String, IdmUser> searchUsernamesUsers(Integer first, Integer max, SearchUsersParams params)
            throws NotAuthorizedException, BadRequestException {
        HashMap<String, String> headers = IdmRestClient.getHeadersWithAuth(this.auth_token, null);

        HashMap<String, String> parameters = getSearchParameters(IdmUser.USERS_REPR.username_user, first, max, params);

        JavaType mapStringType = JsonUtilsCustom.geMapOfObjectsType(String.class, IdmUser.class);
        ResponseBean<Map<String, IdmUser>> resp = this.client.performGetRequest("/users/search", headers,
                parameters, mapStringType);
        return resp.getResult();
    }

    @Override
    public List<String> searchFullnames(Integer first, Integer max, SearchUsersParams params)
            throws NotAuthorizedException, BadRequestException {
        HashMap<String, String> headers = IdmRestClient.getHeadersWithAuth(this.auth_token, null);
        HashMap<String, String> parameters = getSearchParameters(IdmUser.USERS_REPR.fullname, first, max, params);
        JavaType listType = JsonUtilsCustom.geListOfObjectsType(String.class);
        ResponseBean<List<String>> resp = this.client.performGetRequest("/users/search", headers, parameters, listType);
        return resp.getResult();
    }
}
