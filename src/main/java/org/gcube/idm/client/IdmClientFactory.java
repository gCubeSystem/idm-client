package org.gcube.idm.client;

import java.rmi.ServerException;

import org.gcube.common.security.secrets.Secret;
import org.gcube.idm.common.is.InfrastrctureServiceClient;
import org.gcube.idm.common.is.IsServerConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jakarta.ws.rs.InternalServerErrorException;

public class IdmClientFactory {

	private final static String RUNTIME_RESOURCE_NAME = "identity-manager";
	private final static String CATEGORY = "org.gcube.auth";
	private final static String END_POINT_NAME = "d4science";
	private final static boolean IS_ROOT_SERVICE = true;

	protected static final Logger logger = LoggerFactory.getLogger(IdmClientFactory.class);

	/**
	 * keycloak configuration obtained from IS in the private constructor
	 * using the singleton pattern, it's retrieved from IS only for the first
	 * access, then kept in the singleton object
	 */
	private IsServerConfig config;
	private Secret secret;

	// the singleton obj

	private static IdmClientFactory singleton = new IdmClientFactory();

	public static IdmClientFactory getSingleton() {
		if (singleton == null)
			singleton = new IdmClientFactory();
		return singleton;
	}

	public Secret getSecret() {
		return secret;
	}

	// set a custom secret, instead of fetch it from IS
	public void setSecret(Secret secret) {
		this.secret = secret;
		this.setConfig(null);
		// this.setConfig(fetchIsConfig(secret));
	}

	// set a custom config for the factory, skipping or overriding the fetch from IS
	public void setConfig(IsServerConfig config) {
		this.config = config;
	}

	public IsServerConfig fetchIsConfig() throws InternalServerErrorException {
		if (this.secret == null){
			String error_message = "secret is null. invoke setSecret(secret) before fetching the configuration. ";
			throw new InternalServerErrorException(error_message);
		}

		if (this.config == null) {
			this.config = fetchIsConfig(this.secret);
		}

		return fetchIsConfig(this.secret);
	}

	public IsServerConfig fetchIsConfig(Secret secret) throws InternalServerErrorException {
		try {
			//Secret secret = InfrastrctureServiceClient.getSecretForInfrastructure();
			IsServerConfig cfg = InfrastrctureServiceClient.serviceConfigFromIS(RUNTIME_RESOURCE_NAME, CATEGORY,
					END_POINT_NAME, IS_ROOT_SERVICE, secret);
			return cfg;
		} catch (Exception e) {
			e.printStackTrace();
			throw new InternalServerErrorException(e);
		}
	}

	public IdmMeClient meClient(String auth_token) throws ServerException {
		logger.debug("Instantiating a new IdmClient instance");

		logger.info("Building KeycloakAPICredentials object");
		try {
			if (this.config == null) {
				this.config = fetchIsConfig();
			}
			logger.info("KeycloakAPICredentials object built {} - {}", config.getServerUrl(), config.getName());

		} catch (Exception e) {
			logger.error("error obtaining IAM configuration from IS {} ", e);
			throw new ServerException(e.getMessage(), e);
		}

		String url = null;
		try {
			url = this.config.getServerUrl();
			return new DefaultMeClient(url, auth_token);
		} catch (Exception e) {
			logger.error("cannot create idm client with url ", url);

			e.printStackTrace();
			throw new ServerException(e.getMessage(), e);
		}
	}

	public IdmUsersClient userClient(String auth_token) throws ServerException {
		logger.debug("Instantiating a new IdmClient instance");

		logger.info("Building KeycloakAPICredentials object");
		try {
			if (this.config == null) {
				this.config = fetchIsConfig();
			}
			logger.info("KeycloakAPICredentials object built {} - {}", config.getServerUrl(), config.getName());

		} catch (Exception e) {
			logger.error("error obtaining IAM configuration from IS {} ", e);
			throw new ServerException(e.getMessage(), e);
		}

		String url = null;
		try {
			url = this.config.getServerUrl();
			return new DefaultUsersClient(url, auth_token);
		} catch (Exception e) {
			logger.error("cannot create idm client with url ", url);

			e.printStackTrace();
			throw new ServerException(e.getMessage(), e);
		}
	}

}
