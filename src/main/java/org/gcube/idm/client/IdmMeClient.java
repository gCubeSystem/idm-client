package org.gcube.idm.client;

import java.util.List;
import java.util.Map;

import org.gcube.idm.client.model.OwnerInfo;
import org.gcube.idm.client.model.UserInfo;
import org.gcube.idm.client.model.UserInspect;
import org.gcube.idm.client.model.UserProfile;
import org.gcube.idm.common.models.IdmUser;
import org.gcube.idm.common.models.IdmVerifyObject;

import jakarta.ws.rs.BadRequestException;
import jakarta.ws.rs.NotAuthorizedException;

public interface IdmMeClient {

    public UserInfo getMe() throws NotAuthorizedException, BadRequestException;

    public String getMeId() throws NotAuthorizedException, BadRequestException;

    public String getMeEmail() throws NotAuthorizedException, BadRequestException;

    public String getMeUsername() throws NotAuthorizedException, BadRequestException;

    public String getMeName() throws NotAuthorizedException, BadRequestException;

    public Map<String, List<String>> getMeAttributes() throws NotAuthorizedException, BadRequestException;

    // public String getMeAttribute(String attribute) throws NotAuthorizedException, BadRequestException;

    public IdmUser getMeUser() throws NotAuthorizedException, BadRequestException;

    public OwnerInfo getMeOwner() throws NotAuthorizedException, BadRequestException;

    public UserProfile getMeProfile() throws NotAuthorizedException, BadRequestException;

    public IdmVerifyObject verifyToken() throws NotAuthorizedException, BadRequestException;

    public UserInspect getMeInspect() throws NotAuthorizedException, BadRequestException;

}
