package org.gcube.idm.client;

import java.util.List;
import java.util.Map;

import org.gcube.idm.client.model.UserInfo;
import org.gcube.idm.client.model.UserProfile;
import org.gcube.idm.common.models.IdmFullUser;
import org.gcube.idm.common.models.IdmUser;

import jakarta.ws.rs.BadRequestException;
import jakarta.ws.rs.NotAuthorizedException;

public interface IdmUsersClient {
    public String getUserId(String user_id) throws NotAuthorizedException, BadRequestException;

    public String getUserEmail(String user_id) throws NotAuthorizedException, BadRequestException;

    public String getUserUsername(String user_id) throws NotAuthorizedException, BadRequestException;

    public IdmUser getUserOwner(String user_id) throws NotAuthorizedException, BadRequestException;

    public UserProfile getUserProfile(String user_id) throws NotAuthorizedException, BadRequestException;

    public List<String> getAllUsernames() throws NotAuthorizedException, BadRequestException;

    public List<String> getAllUsernames(Integer first, Integer max)throws NotAuthorizedException, BadRequestException;

    public List<IdmUser> searchUsers(Integer first, Integer max, SearchUsersParams params) throws NotAuthorizedException, BadRequestException;

    public List<IdmFullUser> searchFullUsers(Integer first, Integer max, SearchUsersParams params) throws NotAuthorizedException, BadRequestException;

    public List<String> searchUsernames(Integer first, Integer max, SearchUsersParams params) throws NotAuthorizedException, BadRequestException;
    public List<String> searchEmails(Integer first, Integer max, SearchUsersParams params) throws NotAuthorizedException, BadRequestException;
    public Map<String, String> searchUsernamesEmails(Integer first, Integer max, SearchUsersParams params) throws NotAuthorizedException, BadRequestException;
    public Map<String, IdmUser> searchUsernamesUsers(Integer first, Integer max, SearchUsersParams params) throws NotAuthorizedException, BadRequestException;
    public List<String> searchFullnames(Integer first, Integer max, SearchUsersParams params) throws NotAuthorizedException, BadRequestException;

    public UserInfo getUser(String user_id);

}
