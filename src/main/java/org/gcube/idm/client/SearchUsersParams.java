package org.gcube.idm.client;

public class SearchUsersParams {
    public Boolean exact = true;
    public Boolean enabled;
    public String username;
    public String firstName;
    public String lastName;
    public String email;
}
