package org.gcube.idm.client.beans;

// import org.jboss.weld.util.LazyValueHolder.Serializable;

import java.io.Serializable;

import org.gcube.com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Response bean with real result of type T.
 * 
 * @author Costantino Perciante at ISTI-CNR
 *         (costantino.perciante@isti.cnr.it)
 */
@JsonIgnoreProperties
public class ResponseBean<T> implements Serializable {

	private static final long serialVersionUID = -2725238162673879658L;

	protected boolean success;
	protected String message;
	protected T result;

	public ResponseBean() {
		super();
	}

	/**
	 * @param success
	 * @param message
	 * @param result
	 */
	public ResponseBean(boolean success, String message, T result) {
		super();
		this.success = success;
		this.message = message;
		this.result = result;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public T getResult() {
		return result;
	}

	public void setResult(T result) {
		this.result = result;
	}

	@Override
	public String toString() {
		return "ResponseBean [success=" + success
				+ ", message=" + message + ", result=" + result + "]";
	}
}