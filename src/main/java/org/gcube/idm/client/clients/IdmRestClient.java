package org.gcube.idm.client.clients;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.gcube.com.fasterxml.jackson.databind.JavaType;
import org.gcube.common.gxhttp.util.ContentUtils;
import org.gcube.common.gxrest.request.GXHTTPStringRequest;
import org.gcube.common.gxrest.response.inbound.GXInboundResponse;
import org.gcube.idm.client.beans.ResponseBean;
import org.gcube.idm.client.model.util.JsonUtilsCustom;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jakarta.ws.rs.BadRequestException;
import jakarta.ws.rs.NotAuthorizedException;

public class IdmRestClient {
    protected static Logger logger = LoggerFactory.getLogger(IdmRestClient.class);

    protected final static String AUTHORIZATION_HEADER = "Authorization";

    protected URI BASE_URL = null;

    public URI getBASE_URL() {
        return BASE_URL;
    }

    public void setBASE_URL(URI bASE_URL) {
        BASE_URL = bASE_URL;
    }

    public IdmRestClient(String base_url) throws URISyntaxException {
        if (!base_url.endsWith("/")) {
            base_url += "/";
        }
        this.BASE_URL = new URI(base_url);
    }

    public IdmRestClient(URI base_url) {
        this.BASE_URL = base_url;
    }

    public URL getUrl(String extraPath) throws URISyntaxException,
            MalformedURLException {
        if (extraPath.startsWith("/")) {
            extraPath = extraPath.substring(1);
        }

        return BASE_URL.resolve(extraPath).toURL();
    }

    public URI getUri(String extraPath) throws URISyntaxException,
            MalformedURLException {
        return BASE_URL.resolve(extraPath);
    }

    public static String bearerAuth(String token) {
        return "Bearer " + token;
    }

    public static HashMap<String, String> getHeadersWithAuth(String auth_token, HashMap<String, String> headers) {
        if (headers == null)
            headers = new HashMap<String, String>();
        if (auth_token != null) {
            headers.put(AUTHORIZATION_HEADER, bearerAuth(auth_token));
        }

        return headers;
    }

    /**
     * perform a get request. argument type as classtype (TheClass.class)
     * 
     * @param <T>
     * @param relativeUrl
     * @param auth_token
     * @param classtype
     * @return
     */
    public <T> ResponseBean<T> performGetRequest(String relativeUrl, String auth_token, Class<T> classtype) {
        JavaType objectsType = JsonUtilsCustom.getObjectJT(classtype);
        return performGetRequest(relativeUrl, auth_token, objectsType);
    }

    public <T> ResponseBean<T> performGetRequest(String relativeUrl, Map<String, String> headers,
            Map<String, String> params, Class<T> classtype) throws NotAuthorizedException, BadRequestException {
        JavaType jt = JsonUtilsCustom.getObjectJT(classtype);
        return performGetRequest(relativeUrl, headers, params, jt);
    }

    /**
     * 
     * perform a get request. argument type as JavaType ( => JavaType objectsType =
     * JsonUtilsCustom.getObjectJT(TheClass.class) )
     * 
     * when you have a composed java tape from genrics (es HashMap<String, Object>)
     * use the JsonUtilsCustom methods to obtain the relative JavaType
     * 
     * @param auth_token
     * @param headers    // null to create a new HashMap
     * @return
     */
    public <T> ResponseBean<T> performGetRequest(String relativeUrl, String auth_token, JavaType objectsType)
            throws NotAuthorizedException, BadRequestException {
        HashMap<String, String> headers = getHeadersWithAuth(auth_token, null);
        return performGetRequest(relativeUrl, headers, null, objectsType);
    }

    public <T> ResponseBean<T> performGetRequest(String relativeUrl, Map<String, String> headers,
            Map<String, String> params, JavaType objectsType)
            throws NotAuthorizedException, BadRequestException {
        if (relativeUrl == null) {
            throw new BadRequestException("relativeUrl URL must be not null");
        }

        URL url = null;
        try {
            url = getUrl(relativeUrl);
        } catch (Exception e) {
            throw new BadRequestException("Url not acceptable", e);

        }

        GXHTTPStringRequest request = prepareGetRequest(url, headers, params);
        GXInboundResponse response;
        try {
            response = request.get();
        } catch (Exception e) {
            e.printStackTrace();
            throw new BadRequestException("Cannot send request correctly", e);
        }

        try {
            if (response.isSuccessResponse()) {
                JavaType rb_type = JsonUtilsCustom.getResponseBeanOfObjectsType(objectsType);
                return tryConvertStreamedContentFromJson(response, rb_type);
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            throw new BadRequestException("Cannot send request correctly", e);
        }

        if (response.getHTTPCode() == 401) {
            throw new NotAuthorizedException("token invalid");
        }
        throw new BadRequestException("Cannot send request correctly");

    }

    public <T> ResponseBean<T> performPostRequest(String relativeUrl, Map<String, String> headers,
            Map<String, List<String>> params, Class<T> classtype)
            throws NotAuthorizedException, BadRequestException {
        if (relativeUrl == null) {
            throw new BadRequestException("relativeUrl URL must be not null");
        }

        URL url = null;
        try {
            url = getUrl(relativeUrl);
        } catch (Exception e) {
            throw new BadRequestException("Url not acceptable", e);

        }

        GXHTTPStringRequest request = preparePostRequest(url, headers, params);
        GXInboundResponse response;
        try {
            response = request.post();
        } catch (Exception e) {
            throw new BadRequestException("Cannot send request correctly", e);
        }

        try {
            if (response.isSuccessResponse()) {
                JavaType rb_type = JsonUtilsCustom.getResponseBeansObjectJT(classtype);
                return tryConvertStreamedContentFromJson(response, rb_type);
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            throw new BadRequestException("Cannot send request correctly", e);
        }
        throw new BadRequestException("Cannot send request correctly");

    }

    // TODO: coso è?
    public static void safeSetAsExternalCallForOldAPI(GXHTTPStringRequest request) {
        try {
            logger.trace("Looking for the 'isExternalCall' method in the 'GXHTTPStringRequest' class");
            Method isExetnalCallMethod = request.getClass().getMethod("isExternalCall", boolean.class);
            logger.trace("Method found, is the old gxJRS API. Invoking it with 'true' argument");
            isExetnalCallMethod.invoke(request, true);
        } catch (NoSuchMethodException e) {
            logger.trace("Method not found, is the new gxJRS API");
        } catch (SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
            logger.warn("Cannot invoke 'isExternalCall' method via reflection on 'GXHTTPStringRequest' class", e);
        }
    }

    /**
     * Gets the streamed content as a string, if possible.
     * 
     * @return the content
     * @throws IOException
     *                     if unable to read the content
     */
    public static String getStreamedContentAsString(HttpURLConnection response) throws IOException {
        String body = ContentUtils.toString(ContentUtils.toByteArray(response.getInputStream()));
        return body;
    }

    /**
     * Tries to convert the content from its Json serialization, if possible.
     * 
     * @param <T>
     *            the type of the desired object
     * @return an object of type T from the content
     * @throws IOException
     * @throws Exception
     *                     if the deserialization fails
     */
    public static <T> T tryConvertStreamedContentFromJson(HttpURLConnection response, Class<T> classtype)
            throws IOException, Exception {
        return JsonUtilsCustom.fromJson(getStreamedContentAsString(response), classtype);
    }

    public static <T> T tryConvertStreamedContentFromJson(HttpURLConnection response, JavaType objectType)
            throws IOException, Exception {
        return JsonUtilsCustom.fromJson(ContentUtils.toByteArray(response.getInputStream()), objectType);
    }

    public static <T> T tryConvertStreamedContentFromJson(GXInboundResponse response, JavaType objectType)
            throws IOException, Exception {
        String body = response.getStreamedContentAsString();

        return JsonUtilsCustom.fromJson(body, objectType);
    }

    public static <T> T tryConvertStreamedContentFromJson(GXInboundResponse response, Class<T> classtype)
            throws IOException, Exception {
        return JsonUtilsCustom.fromJson(response.getStreamedContentAsString(), classtype);
    }



    public static GXHTTPStringRequest preparePostRequest(
            URL url, Map<String, String> headers,
            Map<String, List<String>> params) throws BadRequestException {

        // if (headers == null || !headers.containsKey(AUTHORIZATION_HEADER)
        // || "".equals(headers.get(AUTHORIZATION_HEADER))) {
        // throw new NotAuthorizedException("Authorization must be not null nor empty");
        // }

        // Constructing request object
        GXHTTPStringRequest request;
        try {

            String queryString = null;
            if (params != null) {
                queryString = params.entrySet().stream()
                        .flatMap(p -> p.getValue().stream().map(v -> p.getKey() + "=" + v))
                        .reduce((p1, p2) -> p1 + "&" + p2).orElse("");
            }

            logger.trace("Query string is {}", queryString);

            request = GXHTTPStringRequest.newRequest(url.toString())
                    .header("Content-Type", "application/x-www-form-urlencoded").withBody(queryString);

            safeSetAsExternalCallForOldAPI(request);

            logger.trace("Adding provided headers: {}", headers);
            for (String headerName : headers.keySet()) {
                request.header(headerName, headers.get(headerName));
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new BadRequestException("Cannot construct the request object correctly", e);
        }

        return request;
    }

    public static GXHTTPStringRequest prepareGetRequest(
            URL url, Map<String, String> headers,
            Map<String, String> params) throws BadRequestException {

        // if (headers == null || !headers.containsKey(AUTHORIZATION_HEADER)
        // || "".equals(headers.get(AUTHORIZATION_HEADER))) {
        // throw new NotAuthorizedException("Authorization must be not null nor empty");
        // }

        // Constructing request object
        GXHTTPStringRequest request;
        try {

            String queryString = null;

            logger.trace("Query string is {}", queryString);

            request = GXHTTPStringRequest.newRequest(url.toString());

            if (params != null) {
                request.queryParams(params);
            }

            safeSetAsExternalCallForOldAPI(request);

            logger.trace("Adding provided headers: {}", headers);
            for (String headerName : headers.keySet()) {
                request.header(headerName, headers.get(headerName));
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new BadRequestException("Cannot construct the request object correctly", e);
        }

        return request;
    }

    
}
