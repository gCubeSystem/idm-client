package org.gcube.idm.client.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class OwnerInfo {

	@JsonProperty("roles")
	private List<String> roles;

	public List<String> getRoles() {
		return roles;
	}

	public void setRoles(List<String> roles) {
		this.roles = roles;
	}

	@JsonProperty("globalRoles")
	private List<String> globalRoles;


	@JsonProperty("externalClient")
	private boolean externalClient;

	@JsonProperty("email")
	private String email;

	@JsonProperty("firstName")
	private String firstName;

	@JsonProperty("lastName")
	private String lastName;

	@JsonProperty("clientName")
	private String clientName;

	@JsonProperty("contactPerson")
	private String contactPerson;

	@JsonProperty("contactOrganisation")
	private String contactOrganisation;

	@JsonProperty("application")
	private String application;

	@JsonProperty("id")
	private String id;

	public OwnerInfo(List<String> roles, List<String> globalRoles, boolean externalClient, String email,
            String firstName, String lastName, String clientName, String contactPerson, String contactOrganisation,
            String application, String id) {
        this.roles = roles;
        this.globalRoles = globalRoles;
        this.externalClient = externalClient;
        this.email = email;
        this.firstName = firstName;
        this.lastName = lastName;
        this.clientName = clientName;
        this.contactPerson = contactPerson;
        this.contactOrganisation = contactOrganisation;
        this.application = application;
        this.id = id;
    }

    public OwnerInfo() {
		super();
	}

	// /**
	// * @param username
	// * @param roles
	// * @param avatar
	// * @param fullname
	// */
	// public UserProfile(String username, List<String> roles, String avatar,
	// String fullname) {
	// super();
	// this.username = username;
	// this.roles = roles;
	// this.avatar = avatar;
	// this.fullname = fullname;
	// }

	@Override
	public String toString() {
		return "UserProfile [id = " + id + ", roles = " + roles + ", gloablRoles = " + globalRoles + "]";
	}

	public boolean isExternalClient() {
		return externalClient;
	}

	public void setExternalClient(boolean externalClient) {
		this.externalClient = externalClient;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getClientName() {
		return clientName;
	}

	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	public String getContactPerson() {
		return contactPerson;
	}

	public void setContactPerson(String contactPerson) {
		this.contactPerson = contactPerson;
	}

	public String getContactOrganisation() {
		return contactOrganisation;
	}

	public void setContactOrganisation(String contactOrganisation) {
		this.contactOrganisation = contactOrganisation;
	}

	public String getApplication() {
		return application;
	}

	public void setApplication(String application) {
		this.application = application;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public List<String> getGlobalRoles() {
		return globalRoles;
	}

	public void setGlobalRoles(List<String> globalRoles) {
		this.globalRoles = globalRoles;
	}
}
