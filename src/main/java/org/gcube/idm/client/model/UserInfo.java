package org.gcube.idm.client.model;

import org.gcube.idm.common.models.IdmUser;

public class UserInfo {
    public OwnerInfo owner;
    public UserProfile profile;
    public IdmUser user;
}