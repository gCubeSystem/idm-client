package org.gcube.idm.client.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class UserProfileExtended extends UserProfile {

    @JsonProperty("middle_name")
    private String middleName;

    @JsonProperty("male")
    private boolean male;

    @JsonProperty("location_industry")
    private String locationIndustry;

    @JsonProperty("first_name")
    private String firstName;

    @JsonProperty("email")
    private String email;

    @JsonProperty("job_title")
    private String jobTitle;

    @JsonProperty("last_name")
    private String lastName;

    @JsonProperty("registration_date")
    private long registrationDate;

    @JsonProperty("user_id")
    private long userId;

    @JsonProperty("email_addresses")
    private List<String> emailAddresses;

    public UserProfileExtended() {
        super();
    }

    /**
     * @param username
     * @param roles
     * @param avatar
     * @param fullname
     */
    public UserProfileExtended(String username, List<String> roles,
            String avatar, String fullname) {
        super(username, roles, avatar, fullname);
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public boolean isMale() {
        return male;
    }

    public void setMale(boolean male) {
        this.male = male;
    }

    public String getLocationIndustry() {
        return locationIndustry;
    }

    public void setLocationIndustry(String locationIndustry) {
        this.locationIndustry = locationIndustry;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public long getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(long registrationDate) {
        this.registrationDate = registrationDate;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public List<String> getEmailAddresses() {
        return emailAddresses;
    }

    public void setEmailAddresses(List<String> emailAddresses) {
        this.emailAddresses = emailAddresses;
    }

    @Override
    public String toString() {
        return "UserProfileExtended [UserProfile =" + super.toString() + ", middleName=" + middleName + ", male="
                + male + ", locationIndustry=" + locationIndustry
                + ", firstName=" + firstName + ", email=" + email
                + ", jobTitle=" + jobTitle + ", lastName=" + lastName
                + ", registrationDate=" + registrationDate + ", userId="
                + userId + ", emailAddresses=" + emailAddresses + "]";
    }

}
