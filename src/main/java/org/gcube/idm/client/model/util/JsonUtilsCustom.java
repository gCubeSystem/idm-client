package org.gcube.idm.client.model.util;

import java.util.List;
import java.util.Map;

import org.gcube.com.fasterxml.jackson.databind.DeserializationFeature;
import org.gcube.com.fasterxml.jackson.databind.JavaType;
import org.gcube.com.fasterxml.jackson.databind.ObjectMapper;
import org.gcube.com.fasterxml.jackson.databind.type.TypeFactory;
import org.gcube.idm.client.beans.ResponseBean;

// cannot extend JsonUtils , it's final
// public class JsonUtilsCustom  extends JsonUtils {

public class JsonUtilsCustom {

    private static TypeFactory _tf = null;
    private static ObjectMapper _om = null;

    private static ObjectMapper getObjectMapper() {
        if (_om == null) {
            _om = new ObjectMapper();
        }
        return _om;

    }

    private static TypeFactory getTypeFactory() {
        if (_tf == null) {
            ObjectMapper om = getObjectMapper();
            _tf = om.getTypeFactory();
        }
        return _tf;
    }

    public static JavaType getObjectJT(Class<?> classType) {
        TypeFactory tf = getTypeFactory();
        // map key type
        JavaType objectsType = tf.constructType(classType);
        return objectsType;
    }

    public static JavaType getResponseBeansObjectJT(Class<?> classType) {
        TypeFactory tf = getTypeFactory();
        // map key type
        JavaType objectsType = tf.constructType(classType);
        return getResponseBeanOfObjectsType(objectsType);
    }

    public static JavaType getResponseBeanOfObjectsType(JavaType objectsType) {
        TypeFactory tf = getTypeFactory();
        JavaType beansType = tf.constructParametrizedType(ResponseBean.class, ResponseBean.class, objectsType);
        return beansType;
    }

    public static JavaType geListOfObjectsType(Class<?> classType) {
        TypeFactory tf = getTypeFactory();
        JavaType objectsType = tf.constructType(classType);
        return geListOfObjectsType(objectsType);
    }

    public static JavaType geListOfObjectsType(JavaType objectsType) {
        TypeFactory tf = getTypeFactory();
        JavaType listOfObject = tf.constructParametrizedType(List.class, List.class, objectsType);
        return listOfObject;
    }

    public static JavaType geMapOfObjectsType(Class<?> keyClass, Class<?> valueClass) {
        TypeFactory tf = getTypeFactory();
        JavaType keyType = tf.constructType(keyClass);
        JavaType valueType = tf.constructType(valueClass);
        return geMapOfObjectsType(keyType, valueType);
    }

    public static JavaType geMapOfObjectsType(JavaType keyType, JavaType valueType) {
        TypeFactory tf = getTypeFactory();
        JavaType mapOfObject = tf.constructParametrizedType(Map.class, Map.class, keyType, valueType);
        return mapOfObject;
    }

    public static JavaType getResponseBeanOfListOfObjectsType(Class<?> classType) {
        JavaType listOfObjectType = geListOfObjectsType(classType);
        return getResponseBeanOfListOfObjectsType(listOfObjectType);
    }

    public static JavaType getResponseBeanOfListOfObjectsType(JavaType listOfObjectType) {
        JavaType beansType = getResponseBeanOfObjectsType(listOfObjectType);
        return beansType;
    }

    /**
     * @param <T>
     * @param bytes
     * @param type
     * @return
     * @throws Exception
     */
    /**
     * Deserializes the specified Json bytes into an object of the specified class
     * 
     * @param <T>      the type of the desired object
     * @param json     the string from which the object is to be deserialized
     * @param classOfT the class of T
     * @return an object of type T from the bytes
     * @throws Exception if the deserialization fails
     */
    public static <T> T fromJson(byte[] bytes, JavaType type) throws Exception {
        try {
            return getObjectMapper().readValue(bytes, type);
        } catch (Exception e) {
            throw new Exception("Cannot deserialize to the object.", e);
        }
    }

    /**
     * Deserializes the specified Json bytes into an object of the specified class
     * 
     * @param <T>      the type of the desired object
     * @param json     the string from which the object is to be deserialized
     * @param classOfT the class of T
     * @return an object of type T from the bytes
     * @throws Exception if the deserialization fails
     */
    public static <T> T responseBeanFromJson(byte[] bytes, JavaType type) throws Exception {
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            return objectMapper.readValue(bytes, type);
        } catch (Exception e) {
            throw new Exception("Cannot deserialize to the object.", e);
        }
    }

    /**
     * Deserializes the specified Json bytes into an object of the specified class
     * 
     * @param <T>      the type of the desired object
     * @param json     the string from which the object is to be deserialized
     * @param classOfT the class of T
     * @return an object of type T from the bytes
     * @throws Exception if the deserialization fails
     */
    public static <T> T fromJson(byte[] bytes, Class<T> raw) throws Exception {
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            return objectMapper.readValue(bytes, raw);
        } catch (Exception e) {
            throw new Exception("Cannot deserialize to the object.", e);
        }
    }

    /**
     * Deserializes the specified Json bytes into an object of the specified class
     * 
     * @param <T>  the type of the desired object
     * @param json the string from which the object is to be deserialized
     * @param raw  the class of T
     * @return an object of type T from the bytes
     * @throws Exception if the deserialization fails
     */
    public static <T> T fromJson(String json, Class<T> raw) throws Exception {
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            return objectMapper.readValue(json, raw);
        } catch (Exception e) {
            throw new Exception("Cannot deserialize to the object.", e);
        }
    }

    /**
     * Deserializes the specified Json bytes into an object of the specified class
     * 
     * @param <T>  the type of the desired object
     * @param json the string from which the object is to be deserialized
     * @param raw  the class of T
     * @return an object of type T from the bytes
     * @throws Exception if the deserialization fails
     */
    public static <T> T fromJson(String json, JavaType objectType) throws Exception {
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

            return objectMapper.readValue(json, objectType);
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Cannot deserialize to the object.", e);
        }
    }

}
