package org.gcube.idm.client.model.util;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import org.gcube.com.fasterxml.jackson.core.JsonParser;
import org.gcube.com.fasterxml.jackson.databind.DeserializationContext;
import org.gcube.com.fasterxml.jackson.databind.JsonDeserializer;
import org.gcube.com.fasterxml.jackson.databind.JsonNode;

public class StringOrArrayDeserializer extends JsonDeserializer<Object> {

    @Override
    public Object deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        JsonNode jsonNode = jsonParser.readValueAsTree();
        if (jsonNode.isArray()) {
            ArrayList<String> a = new ArrayList<>(1);
            Iterator<JsonNode> itr = jsonNode.iterator();
            while (itr.hasNext()) {
                a.add(itr.next().textValue());
            }
            return a.toArray(new String[a.size()]);
        } else {
            return new String[] { jsonNode.textValue() };
        }
    }

}
