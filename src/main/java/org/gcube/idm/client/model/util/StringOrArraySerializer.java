package org.gcube.idm.client.model.util;

import java.io.IOException;

import org.gcube.com.fasterxml.jackson.core.JsonGenerator;
import org.gcube.com.fasterxml.jackson.databind.JsonSerializer;
import org.gcube.com.fasterxml.jackson.databind.SerializerProvider;

public class StringOrArraySerializer extends JsonSerializer<Object> {
    @Override
    public void serialize(Object o, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        String[] array = (String[]) o;
        if (array == null) {
            jsonGenerator.writeNull();
        } else if (array.length == 1) {
            jsonGenerator.writeString(array[0]);
        } else {
            jsonGenerator.writeStartArray();
            for (String s : array) {
                jsonGenerator.writeString(s);
            }
            jsonGenerator.writeEndArray();
        }
    }
}
