package org.gcube.idm.client.test;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.rmi.ServerException;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.gcube.com.fasterxml.jackson.core.type.TypeReference;
import org.gcube.com.fasterxml.jackson.databind.ObjectMapper;
import org.gcube.common.gxrest.request.GXHTTPStringRequest;
import org.gcube.common.gxrest.response.inbound.GXInboundResponse;
import org.gcube.common.keycloak.KeycloakClientException;
import org.gcube.common.keycloak.KeycloakClientFactory;
import org.gcube.common.keycloak.KeycloakClientHelper;
import org.gcube.common.keycloak.model.TokenResponse;
import org.gcube.common.security.providers.SecretManagerProvider;
import org.gcube.common.security.secrets.AccessTokenSecret;
import org.gcube.common.security.secrets.Secret;
import org.gcube.idm.client.IdmClientFactory;
import org.gcube.idm.client.IdmMeClient;
import org.gcube.idm.client.IdmUsersClient;
import org.gcube.idm.client.clients.IdmRestClient;
import org.gcube.idm.common.is.IsServerConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jakarta.ws.rs.BadRequestException;

/**
 * @author Luca Frosini (ISTI - CNR)
 *
 */
public abstract class ClientContextTest {

	private static final Logger logger = LoggerFactory.getLogger(ClientContextTest.class);

	protected static final String CONFIG_INI_FILENAME = "test.ini";

	public static final String DEFAULT_TEST_SCOPE;

	protected static Secret current_secret;


	public IsServerConfig getMokedIsConfig() {
		String baseurl = properties.getProperty("SERVICE_URL"); // "http://146.48.85.179:9999/idm-service/";
		// String baseurl = "http://localhost:8080/idm-service";
		IsServerConfig config = new IsServerConfig(baseurl);
		return config;
	}

	public IdmMeClient getMeClient() throws IOException {
		String token = getUserToken();
		return getMeClient(token);
	}

	public IdmMeClient getMeClient(String token) throws ServerException {
		IdmClientFactory factory = IdmClientFactory.getSingleton();
		factory.setConfig(getMokedIsConfig());
		IdmMeClient client = factory.meClient(token);
		return client;
	}

	public IdmUsersClient getUserClient() throws IOException {
		String token = getServiceToken();
		return getUserClient(token);
	}

	public IdmUsersClient getUserClient(String token) throws ServerException {

		IdmClientFactory factory = IdmClientFactory.getSingleton();
		factory.setConfig(getMokedIsConfig());
		IdmUsersClient client = factory.userClient(token);
		return client;
	}

	private String service_token;

	public String getServiceToken() throws IOException {
		if (service_token == null) {
			service_token = loginService();
		}

		return service_token;
	}

	private String user_token;

	public String getUserToken() throws IOException {
		if (user_token == null) {
			user_token = loginUser();
		}

		return user_token;
	}

	public String getExpiredServiceToken() {
		String token = properties.getProperty("EXPIRED_SERVICE_TOKEN");
		return token;
	}

	public String getExpiredUserToken() {
		String token = properties.getProperty("EXPIRED_USER_TOKEN");
		return token;
	}

	public static final String VRE;

	protected static final Properties properties;

	static {

		try {
			properties = readProperties(CONFIG_INI_FILENAME);
			VRE = properties.getProperty("CONTEXT");
			DEFAULT_TEST_SCOPE = VRE;

		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	public static Properties readProperties(String filename) throws IOException {

		Properties p = new Properties();
		InputStream input = ClientContextTest.class.getClassLoader().getResourceAsStream(CONFIG_INI_FILENAME);
		// load the properties file
		p.load(input);
		return p;

	}

	private enum Type {
		USER, CLIENT_ID
	};

	public static void set(Secret secret) {
		SecretManagerProvider.reset();
		SecretManagerProvider.set(secret);
		current_secret = secret;
	}

	public static void setContextByName(String fullContextName) throws ServerException {
		logger.debug("Going to set credentials for context {}", fullContextName);
		Secret secret = getSecretByContextName(fullContextName);
		set(secret);
	}

	private static TokenResponse getJWTAccessToken(String context) throws ServerException, KeycloakClientException {
		Type type = Type.valueOf(properties.get("CLIENT_TYPE").toString());

		TokenResponse tr = null;

		int index = context.indexOf('/', 1);
		String root = context.substring(0, index == -1 ? context.length() : index);

		switch (type) {
			case CLIENT_ID:
				String clientId = properties.getProperty("CLIENT_ID");
				String clientSecret = properties.getProperty(root);

				try {
					tr = KeycloakClientFactory.newInstance().queryUMAToken(context, clientId, clientSecret, context,
							null);
				} catch (KeycloakClientException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					throw new ServerException(e.getMessage());
				}
				break;

			case USER:
			default:
				String username = properties.getProperty("LOGIN_USER_USERNAME");
				String password = properties.getProperty("LOGIN_USER_PASS");

				switch (root) {
					case "/gcube":
					default:
						clientId = "next.d4science.org";
						break;

					case "/pred4s":
						clientId = "pre.d4science.org";
						break;

					case "/d4science.research-infrastructures.eu":
						clientId = "services.d4science.org";
						break;
				}
				clientSecret = null;

				tr = KeycloakClientHelper.getTokenForUser(context, username, password);
				break;

		}

		return tr;

	}

	public static Secret getSecretByContextName(String context) throws ServerException {
		TokenResponse tr;
		try {
			tr = getJWTAccessToken(context);
		} catch (KeycloakClientException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new ServerException(e.getMessage());
		}
		Secret secret = new AccessTokenSecret(tr.getAccessToken(), context);
		return secret;
	}

	public static void setContext(String token) {
		Secret secret = getSecret(token);
		set(secret);
	}

	private static Secret getSecret(String token) {
		// TODO: verificare classe (AccessTokenSecret anziche JWTToken) e context(VRE)
		Secret secret = new AccessTokenSecret(token, VRE);
		return secret;
	}

	public static String getOwnerID() {
		String user = "UNKNOWN";
		try {
			user = SecretManagerProvider.get().getOwner().getId();
		} catch (Exception e) {
			logger.error("Unable to retrieve user. {} will be used", user);
		}
		return user;
	}

	public String loginService() throws IOException {
		String context = properties.getProperty("LOGIN_CONTEXT");
		return loginService(context);
	}

	public String loginService(String context) throws IOException {

		URL login_url = null;
		try {
			login_url = new URL(properties.getProperty("LOGIN_URL"));
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		String clientId = properties.getProperty("CLIENT_ID");
		String clientSecret = properties.getProperty("CLIENT_SECRET");

		String encoded_context = context.replace("/", "%2F");

		Map<String, String> headers = new HashMap<String, String>();
		Map<String, List<String>> params = new HashMap<String, List<String>>();
		headers.put("Content-Type", "application/x-www-form-urlencoded");
		headers.put("X-D4Science-Context", encoded_context);

		params.put("client_id", Collections.singletonList(clientId));
		params.put("client_secret", Collections.singletonList(clientSecret));
		params.put("grant_type", Collections.singletonList("client_credentials"));

		GXHTTPStringRequest request = IdmRestClient.preparePostRequest(login_url, headers, params);
		GXInboundResponse response;
		try {
			response = request.post();
		} catch (Exception e) {
			throw new BadRequestException("Cannot send request correctly", e);
		}
		ObjectMapper om = new ObjectMapper();
		TypeReference<Map<String, String>> typeRef = new TypeReference<Map<String, String>>() {
		};
		String jsonstring = response.getStreamedContentAsString();
		HashMap<String, String> result = om.readValue(jsonstring, typeRef);

		return result.get("access_token");
		// return properties.getProperty("TOKEN_USER");

	}

	public String loginUser() throws IOException {

		URL login_url = null;
		try {
			login_url = new URL(properties.getProperty("LOGIN_URL"));
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		String context = properties.getProperty("LOGIN_CONTEXT");
		String encoded_context = context.replace("/", "%2F");

		Map<String, String> headers = new HashMap<String, String>();
		Map<String, List<String>> params = new HashMap<String, List<String>>();
		headers.put("Content-Type", "application/x-www-form-urlencoded");
		headers.put("X-D4Science-Context", encoded_context);

		params.put("client_id", Collections.singletonList(properties.getProperty("LOGIN_USER_CLIENT_ID")));
		params.put("username", Collections.singletonList(properties.getProperty("LOGIN_USER_USERNAME")));
		params.put("password", Collections.singletonList(properties.getProperty("LOGIN_USER_PASS")));
		params.put("grant_type", Collections.singletonList("password"));
		params.put("client_secret", Collections.singletonList(properties.getProperty("LOGIN_USER_CLIENT_SECRET")));

		GXHTTPStringRequest request = IdmRestClient.preparePostRequest(login_url, headers, params);
		GXInboundResponse response;
		try {
			response = request.post();
		} catch (Exception e) {
			throw new BadRequestException("Cannot send request correctly", e);
		}
		ObjectMapper om = new ObjectMapper();
		TypeReference<Map<String, String>> typeRef = new TypeReference<Map<String, String>>() {
		};
		String jsonstring = response.getStreamedContentAsString();
		HashMap<String, String> result = om.readValue(jsonstring, typeRef);

		return result.get("access_token");
		// return properties.getProperty("TOKEN_USER");

	}

}
