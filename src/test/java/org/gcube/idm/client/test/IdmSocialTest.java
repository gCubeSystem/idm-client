package org.gcube.idm.client.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;

import java.io.IOException;
import java.rmi.ServerException;
import java.util.List;

import org.gcube.idm.client.IdmUsersClient;
import org.gcube.idm.client.SearchUsersParams;
import org.gcube.idm.common.models.IdmFullUser;
import org.gcube.idm.common.models.IdmUser;
import org.junit.Test;

import jakarta.ws.rs.NotAuthorizedException;

public class IdmSocialTest extends ClientContextTest {

    // @Test
    public void getPepoleProfile() throws ServerException {
        fail("Unimplemented");

        // IdmUsersClient client = getUserClient();
        // String email = client.getUserEmail("alfredo.oliviero");
        // String expected = "alfredo.oliviero@isti.cnr.it";
        // assertEquals(expected, email);
    }
    // public String getUserEmail(String user_id);
    // public String getUserUsername(String user_id);
    // public TokenInfo getUserOwner(String user_id);
    // public UserProfile getUserProfile(String user_id);

    // @Test
    public void getUserProfile() throws ServerException {
        fail("Unimplemented");

    }

    // @Test
    public void getUser() throws IOException {
        // String user_id = properties.getProperty("USER_ID");

        // IdmUsersClient client = getUserClient();
        // UserInfo resp = client.getUser(user_id);
        // assertNotNull(resp);

        // String expected = "andrea.rossi";
        // assertEquals(expected, resp.user.getUsername());
        // assertEquals(expected, resp.owner.getId());
        // assertEquals(expected, resp.profile.getUsername());
    }

    // @Test
    public void getUserEmail() throws ServerException {
        fail("Unimplemented");
    }

    // @Test
    public void getUserFullname() throws ServerException {
        fail("Unimplemented");
    }

    // @Test
    public void getAllUsernames() throws ServerException {
        fail("Unimplemented");
    }

    // @Test
    public void getAllFullnamesUsernames() throws ServerException {
        fail("Unimplemented");
    }

    // @Test
    public void getUsernamesByRole() throws ServerException {
        fail("Unimplemented");
    }

    // @Test
    public void checkUserExists() throws ServerException {
        fail("Unimplemented");
    }

    // @Test
    public void getCustomAttribute() throws ServerException {
        fail("Unimplemented");
    }

    // @Test
    public void getOAuthProfile() throws ServerException {
        fail("Unimplemented");
    }

    // @Test
    public void getUsernamesByGlobalRole() throws ServerException {
        fail("Unimplemented");
    }

    @Test
    public void searchUsers() throws IOException {
        IdmUsersClient client = getUserClient();
        List<IdmUser> users = client.searchUsers(null, 3, null);
        assertNotNull("expected to receive users", users);
        assertEquals("size expected 3", 3, users.size());
    }

    // @Test
    public void searchFullUsers() throws Exception {
        IdmUsersClient client = getUserClient();
        List<IdmFullUser> users = client.searchFullUsers(null, 3, null);
        assertNotNull("expected to receive users", users);
        assertEquals("size expected 3", 3, users.size());
    }

    @Test
    public void searchFilteredFullUsers() throws IOException {
        IdmUsersClient client = getUserClient();
        SearchUsersParams params = new SearchUsersParams();
        params.email = "alfredo.oliviero";
        params.exact = false;
        List<IdmFullUser> users = client.searchFullUsers(null, 1, params);
        assertNotNull("expected to receive users", users);
        assertEquals("size expected 1", 1, users.size());
        assertEquals("email expected alfredo.oliviero@isti.cnr.it", "alfredo.oliviero@isti.cnr.it",
                users.get(0).getEmail());
    }

    @Test
    public void checkExpireServiceToken() throws ServerException {
        String expired_user_token = getExpiredServiceToken();
        IdmUsersClient client = getUserClient(expired_user_token);
        String email = null;
        try {
            client.getUserEmail(properties.getProperty("USER_ID"));
        } catch (NotAuthorizedException e) {
            return;
        }

        assertNull("expired token, email should be null", email);
    }

}
