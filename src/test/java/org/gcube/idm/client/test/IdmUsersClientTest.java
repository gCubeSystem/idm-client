package org.gcube.idm.client.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.io.IOException;
import java.rmi.ServerException;
import java.util.List;
import java.util.Map;

import org.gcube.idm.client.IdmUsersClient;
import org.gcube.idm.client.SearchUsersParams;
import org.gcube.idm.client.model.UserInfo;
import org.gcube.idm.client.model.UserProfile;
import org.gcube.idm.common.models.IdmFullUser;
import org.gcube.idm.common.models.IdmUser;
import org.junit.Test;

import jakarta.ws.rs.NotAuthorizedException;

public class IdmUsersClientTest extends ClientContextTest {
    @Test
    public void getUser() throws IOException {
        IdmUsersClient client = getUserClient();
        UserInfo user = client.getUser("alfredo.oliviero");
        String expected = "alfredo.oliviero@isti.cnr.it";
        assertEquals(expected, user.user.getEmail());
    }

    // @Test
    // public void getUserInspect() throws IOException {
    // fail("Unimplemented");
    // }

    @Test
    public void getUserProfile() throws IOException {
        IdmUsersClient client = getUserClient();
        UserProfile profile = client.getUserProfile("alfredo.oliviero");
        String expected = "alfredo.oliviero";
        assertEquals(expected, profile.getUsername());
    }

    @Test
    public void getUserEmail() throws IOException {
        IdmUsersClient client = getUserClient();
        String email = client.getUserEmail("alfredo.oliviero");
        String expected = "alfredo.oliviero@isti.cnr.it";
        assertEquals(expected, email);
    }

    // @Test
    // public void getUserRolesRealm() throws IOException {
    // fail("Unimplemented");
    // }

    // @Test
    // public void getUserRolesClient() throws IOException {
    // fail("Unimplemented");
    // }

    // @Test
    // public void getUserGroups() throws IOException {
    // fail("Unimplemented");
    // }

    // @Test
    // public void getUserId() throws IOException {
    // fail("Unimplemented");
    // }

    // @Test
    // public void getUserUsername() throws IOException {
    // fail("Unimplemented");
    // }

    // @Test
    // public void getUserName() throws IOException {
    // fail("Unimplemented");
    // }

    // @Test
    // public void getUserAttributes() throws IOException {
    // fail("Unimplemented");
    // }

    // @Test
    // public void getUserUser() throws IOException {
    // fail("Unimplemented");
    // }

    // public String getUserEmail(String user_id);
    // public String getUserUsername(String user_id);
    // public TokenInfo getUserOwner(String user_id);
    // public UserProfile getUserProfile(String user_id);

    @Test
    public void searchUsernames() throws IOException {
        IdmUsersClient client = getUserClient();
        List<String> usernames = client.searchUsernames(null, 3, null);
        assertNotNull("expected to receive username", usernames);
        assertEquals("size expected 3", 3, usernames.size());
    }

    @Test
    public void searchUsers() throws IOException {
        IdmUsersClient client = getUserClient();
        List<IdmUser> users = client.searchUsers(null, 3, null);
        assertNotNull("expected to receive users", users);
        assertEquals("size expected 3", 3, users.size());
    }

    @Test
    public void searchFullUsers() throws IOException {
        IdmUsersClient client = getUserClient();
        List<IdmFullUser> users = client.searchFullUsers(null, 3, null);
        assertNotNull("expected to receive users", users);
        assertEquals("size expected 3", 3, users.size());
    }
    @Test
    public void searchEmail() throws IOException {
        IdmUsersClient client = getUserClient();
        List<String> emails = client.searchEmails(null, 3, null);
        assertNotNull("expected to receive emails", emails);
        assertEquals("size expected 3", 3, emails.size());
    }

    @Test
    public void searchUsernamesEmail() throws IOException {
        IdmUsersClient client = getUserClient();
       Map<String, String> users_emails = client.searchUsernamesEmails(null, 3, null);
        assertNotNull("expected to receive users", users_emails);
        assertEquals("size expected 3", 3, users_emails.size());
    }
    @Test
    public void searchUsernamesUsers() throws IOException {
        IdmUsersClient client = getUserClient();
         Map<String, IdmUser> users = client.searchUsernamesUsers(null, 3, null);
        assertNotNull("expected to receive users", users);
        assertEquals("size expected 3", 3, users.size());
    }    @Test

    public void searchFullnames() throws IOException {
        IdmUsersClient client = getUserClient();
        List<String> users = client.searchFullnames(null, 3, null);
        assertNotNull("expected to receive users", users);
        assertEquals("size expected 3", 3, users.size());
    }


    @Test
    public void searchFilteredFullUsers() throws IOException {
        IdmUsersClient client = getUserClient();
        SearchUsersParams params = new SearchUsersParams();
        params.email = "alfredo.oliviero";
        params.exact = false;
        List<IdmFullUser> users = client.searchFullUsers(null, 1, params);
        assertNotNull("expected to receive users", users);
        assertEquals("size expected 1", 1, users.size());
        assertEquals("email expected alfredo.oliviero@isti.cnr.it", "alfredo.oliviero@isti.cnr.it",
                users.get(0).getEmail());
    }

    @Test
    public void checkExpireServiceToken() throws ServerException {
        String expired_user_token = getExpiredServiceToken();
        IdmUsersClient client = getUserClient(expired_user_token);
        String email = null;
        try {
            client.getUserEmail(properties.getProperty("USER_ID"));
        } catch (NotAuthorizedException e) {
            return;
        }

        assertNull("expired token, email should be null", email);
    }

}
