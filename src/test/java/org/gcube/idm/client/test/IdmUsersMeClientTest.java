package org.gcube.idm.client.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.gcube.idm.client.IdmMeClient;
import org.gcube.idm.client.model.UserInfo;
import org.gcube.idm.client.model.UserInspect;
import org.gcube.idm.client.model.OwnerInfo;
import org.gcube.idm.client.model.UserProfile;
import org.gcube.idm.common.models.IdmUser;
import org.gcube.idm.common.models.IdmVerifyObject;
import org.junit.Test;

public class IdmUsersMeClientTest extends ClientContextTest {

    @Test
    public void getMe() throws IOException {
        IdmMeClient client = getMeClient();
        UserInfo resp = client.getMe();
        assertNotNull(resp);

        String expected = "andrea.rossi";
        assertEquals(expected, resp.user.getUsername());
        assertEquals(expected, resp.owner.getId());
        assertEquals(expected, resp.profile.getUsername());
    }

    @Test
    public void getMeInspect() throws IOException {
        IdmMeClient client = getMeClient();
        UserInspect resp = client.getMeInspect();
        assertNotNull(resp);
        assertNotNull(resp);

        String email = resp.user.getEmail();
        String expected = "m.assante@gmail.com";
        assertEquals(expected, email);
    }

    @Test
    public void getMeOwner() throws IOException {
        IdmMeClient client = getMeClient();
        OwnerInfo owner = client.getMeOwner();

        assertNotNull(owner);
        assertEquals(properties.getProperty("USER_EMAIL"), owner.getEmail());
        assertEquals(properties.getProperty("USER_ID"), owner.getId());

    }

    @Test
    public void verifyToken() throws IOException {
        IdmMeClient client = getMeClient();
        IdmVerifyObject verify = client.verifyToken();
        assertNotNull(verify);
        // assertEquals(properties.getProperty("USER_ID"), profile.getUsername());
    }

    @Test
    public void getMeProfile() throws IOException {
        IdmMeClient client = getMeClient();
        UserProfile profile = client.getMeProfile();
        assertNotNull(profile);
        assertEquals(properties.getProperty("USER_ID"), profile.getUsername());
    }

    @Test
    public void getMeEmail() throws IOException {
        IdmMeClient client = getMeClient();
        String email = client.getMeEmail();
        String expected = "m.assante@gmail.com";
        assertEquals(expected, email);
    }

    // @Test
    // public void getMeRolesRealm() throws IOException {
    //     fail("Unimplemented");
    // }

    // @Test
    // public void getMeRolesClient() throws IOException {
    //     fail("Unimplemented");
    // }

    // @Test
    // public void getMeGroups() throws IOException {
    //     fail("Unimplemented");
    // }

    @Test
    public void getMeId() throws IOException {
        IdmMeClient client = getMeClient();
        String id = client.getMeId();
        String expected = "771f6151-00ae-45c2-a754-f0546d98f482";
        assertEquals(expected, id);
    }

    @Test
    public void getMeUsername() throws IOException {
        IdmMeClient client = getMeClient();
        String id = client.getMeUsername();
        String expected = "andrea.rossi";
        assertEquals(expected, id);
    }

    @Test
    public void getMeName() throws IOException {
        IdmMeClient client = getMeClient();
        String name = client.getMeName();
        String expected = "Andrea Rossi";
        assertEquals(expected, name);
    }

    @Test
    public void getMeAttributes() throws IOException {
        IdmMeClient client = getMeClient();
        Map<String, List<String>> attributes = client.getMeAttributes();
        assertEquals("attributes #", 12, attributes.size());

        String expected = "Argentina";
        List<String> country_list = attributes.get("country");
        assertEquals("country #", 1, country_list.size());
        assertEquals(expected, country_list.get(0));
    }

    // @Test
    // public void getMeAttribute() throws IOException {
    // String expected = "Argentina";
    // List<String> country_list = attributes.get("country");
    // assertEquals("country #", 1, country_list.size());
    // assertEquals(expected, country_list.get(0));
    // }

    @Test
    public void getMeUser() throws IOException {
        IdmMeClient client = getMeClient();
        IdmUser resp = client.getMeUser();
        assertNotNull(resp);

        String expected = "andrea.rossi";
        assertEquals(expected, resp.getUsername());
    }

}
