package org.gcube.idm.client.test;

import static org.junit.Assert.assertNotNull;

import org.gcube.idm.client.IdmClientFactory;
import org.gcube.idm.common.is.InfrastrctureServiceClient;
import org.gcube.idm.common.is.IsServerConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jakarta.ws.rs.NotFoundException;

public class IsTest extends ClientContextTest {
	private static final Logger logger = LoggerFactory.getLogger(IsTest.class);

	IsServerConfig config = new IsServerConfig("http://localhost:8080/idm-service");

	
	// @Test
	// public void checkContext() throws Exception {
	// ContextTest.setContextByName("gcube/devsec/devVRE");

	// ApplicationContext ctx = ContextProvider.get();
	// ContainerContext container = ctx.container();
	// ContainerConfiguration configuration = container.configuration();

	// String infra_context = "/" + configuration.infrastructure();
	// logger.debug("Testing Keycloak service IS config");

	// }


    //@Test
    public void testIsIDMConfig() throws Exception {

        String RUNTIME_RESOURCE_NAME = "identity-manager";
        String CATEGORY = "org.gcube.auth";
        String END_POINT_NAME = "d4science";
        boolean IS_ROOT_SERVICE = true;

        IsServerConfig cfg = InfrastrctureServiceClient.serviceConfigFromIS(RUNTIME_RESOURCE_NAME, CATEGORY,
                END_POINT_NAME, IS_ROOT_SERVICE, current_secret);

        assertNotNull(cfg);
    }
	
	// @Test
	public void testIsIDM() throws Exception {

		logger.debug("Testing Keycloak service IS config");
		IsServerConfig config = new IsServerConfig("http://localhost:8080/idm-service");

		IdmClientFactory factory = IdmClientFactory.getSingleton();
		factory.setConfig(config);
		org.junit.Assert.assertNotNull(factory);

		try {
			config = factory.fetchIsConfig();
		} catch (NotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		org.junit.Assert.assertNotNull(config);

		logger.debug("fetched IDM service IS config ");
	}



}
